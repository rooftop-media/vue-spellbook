const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
  entry: './spellbook/index.js',
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'spellbook.bundle.js',
    library: 'vue-spellbook',
    libraryTarget: 'commonjs2'
  },
  module: {
    rules: [
      { test: /\.vue$/, use: 'vue-loader' },
      { test: /\.js$/, loader: 'babel-loader' },
      { test: /\.css$/, use: [
        'vue-style-loader',
        'css-loader'
      ]}
    ],
  },
  plugins: [
    // make sure to include the plugin!
    new VueLoaderPlugin()
  ],
};