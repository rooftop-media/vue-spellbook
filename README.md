# Vue Spellbook

Adaptable, reusable material design components for Vue 3.



### Updating Vue-Spellbook

To update the Vue-Spellbook library:

 1. Make the changes you're trying to make. 
 1. Enter the command `npm run build` in your command line
 1. Use Git to add and commit changes, push to the Gitlab repo. 
 1. Use `npm version` to check the package version, update it with `npm version 1.0.x`
 1. Run `npm publish` to publish your updates.
 1. Finally, run `npm install` in repos that use vue spellbook to get an updated version.